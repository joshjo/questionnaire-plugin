$(function() {
    setHighchartsOptions();
    initHomeParallax();
    initFAQs();
    initNavigationWheel();
    initBuyEasyfolioLinks();
    initEasy30();
    initEasy50();
    initEasy70();
    initNewsletterPopup();
    initNewsletterSlide();
    initFormCheckboxes();
    initMobileNavigation();
    initQuestionnaire();
});

var URL_GET_INVESTMENT_LEVEL = 'http://dev.api.easyfolio.de/ajax_get_investment_level/';

var monthShortNames = [
    "Jan",
    "Feb",
    "MÃ¤r",
    "Apr",
    "Mai",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Okt",
    "Nov",
    "Dez"
];

var weekdayNames = [
    "Sonntag",
    "Montag",
    "Dienstag",
    "Mittwoch",
    "Donnerstag",
    "Freitag",
    "Samstag"
];

function setHighchartsOptions() {
    Highcharts.setOptions({
        lang: {
            decimalPoint: ",",
            loading: "Laden ...",
            months: ["Januar", "Februar", "MÃ¤rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
            resetZoom: "Zoom zurÃ¼cksetzen",
            shortMonths: ["Jan.", "Feb.", "MÃ¤r.", "Apr.", "Mai", "Jun.", "Jul.", "Aug.", "Sep.", "Okt.", "Nov.", "Dez."],
            thousandsSep: ".",
            weekdays: ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Februar", "Samstag", "Sonntag"]
        }
    });
}

function initHomeParallax() {
    var scrollorama = $.scrollorama({
        blocks: '.parallax',
        enablePin: false
    });
}

function initFAQs() {
    $('.entryList .faqEntry').hide();
    $('.categoryList .faqCategory h4').click(function() {
        var $button = $(this);
        $button.parent().parent().find('h4').removeClass("active");
        $button.addClass("active");
        $('.entryList .faqEntry[data-alias=' + $button.attr('data-category') + ']').slideDown(500);
        $('.entryList .faqEntry[data-alias!=' + $button.attr('data-category') + ']').slideUp(500);
    });

    $('.entryList .faqEntry h5').click(function() {
        var $button = $(this);
        var $entry = $button.parent();
        $entry.toggleClass("active");
        $entry.find('.content').slideToggle(500);
    });

    var $firstCategory = $('.categoryList .faqCategory:first h4');
    $firstCategory.addClass("active");
    $('.entryList .faqEntry[data-alias=' + $firstCategory.attr('data-category') + ']').slideDown(500);
}

function initNavigationWheel() {

    $(window).load(function() {
        var svg = document.getElementById("navigation");

        if(svg == undefined) {
            return;
        }

        var doc = svg.contentDocument;

        var $body = $('body');
        var $svg = $(doc);

        var warumEinfachURL = "/ueber-easyfolio/das-ist-easyfolio/einfach.html";
        var warumGuenstigURL = "/ueber-easyfolio/das-ist-easyfolio/guenstig.html";
        var warumTransparenzURL = "/ueber-easyfolio/das-ist-easyfolio/transparenz.html";
        var warumSicherURL = "/ueber-easyfolio/das-ist-easyfolio/sicher.html";
        var warumFlexibelURL = "/ueber-easyfolio/das-ist-easyfolio/flexibel.html";

        var warumEinfachSW = $svg.find('#Shape1_sw');
        var warumGuenstigSW = $svg.find('#Shape2_sw');
        var warumTransparenzSW = $svg.find('#Shape3_sw');
        var warumSicherSW = $svg.find('#Shape4_sw');
        var warumFlexibelSW = $svg.find('#Shape_5_sw');

        var warumEinfachColor = $svg.find("#Shape1_cÃ¶our");
        var warumGuenstigColor = $svg.find("#Guenstig_colour");
        var warumTransparenzColor = $svg.find("#Flexibel_colour");
        var warumSicherColor = $svg.find("#Sicher_colour");
        var warumFlexibelColor = $svg.find("#Transparent_colour");

        var warumEinfachText = $svg.find('#einfachText');
        var warumGuenstigText = $svg.find('#guenstigText');
        var warumTransparenzText = $svg.find('#transparentText');
        var warumSicherText = $svg.find('#sicherText');
        var warumFlexibelText = $svg.find('#flexibelText');
        var descriptionText = $svg.find('#descriptionText');

        var warumEinfachHover = $svg.find('#Shape1_x5F_Group_1_');
        var warumGuenstigHover = $svg.find('#Shape2_x5F_Group_1_');
        var warumTransparentHover = $svg.find('#Shape3_x5F_Group_1_');
        var warumSicherHover = $svg.find('#Shape4_x5F_Group_1_');
        var warumFlexibelHover = $svg.find('#Shape5_x5F_Group_2_');

        var currentText = descriptionText;
        var activeText = currentText;

        if($body.hasClass('einfach')) {
            warumEinfachSW.hide();
            currentText = warumEinfachText;
            activeText = warumEinfachText;
        }
        if($body.hasClass('guenstig')) {
            warumGuenstigSW.hide();
            currentText = warumGuenstigText;
            activeText = warumGuenstigText;
        }
        if($body.hasClass('transparenz')) {
            warumTransparenzSW.hide();
            currentText = warumTransparenzText;
            activeText = warumTransparenzText;
        }
        if($body.hasClass('sicher')) {
            warumSicherSW.hide();
            currentText = warumSicherText;
            activeText = warumSicherText;
        }
        if($body.hasClass('flexibel')) {
            warumFlexibelSW.hide();
            currentText = warumFlexibelText;
            activeText = warumFlexibelText;
        }

        currentText.show();

        warumEinfachSW.mouseover(function() {
            warumEinfachHover.show();

            // Replace middle Text
            currentText.hide();
            warumEinfachText.show();
            currentText = warumEinfachText;

            warumEinfachHover.css('cursor', 'pointer');
            warumEinfachHover.click(function() {
                location.href = warumEinfachURL;
            });
            warumEinfachHover.mouseout(function() {
                warumEinfachHover.hide();
                warumEinfachHover.unbind('click');
            });
        });

        warumEinfachColor.mouseover(function() {
            // Replace middle Text
            currentText.hide();
            warumEinfachText.show();
            currentText = warumEinfachText;
        });

        warumGuenstigSW.mouseover(function() {
            warumGuenstigHover.show();

            // Replace middle Text
            currentText.hide();
            warumGuenstigText.show();
            currentText = warumGuenstigText;

            warumGuenstigHover.css('cursor', 'pointer');
            warumGuenstigHover.click(function() {
                location.href = warumGuenstigURL;
            });
            warumGuenstigHover.mouseout(function() {
                warumGuenstigHover.hide();
                warumGuenstigHover.unbind('click');
            });
        });

        warumGuenstigColor.mouseover(function() {
            // Replace middle Text
            currentText.hide();
            warumGuenstigText.show();
            currentText = warumGuenstigText;
        });

        warumTransparenzSW.mouseover(function() {
            warumTransparentHover.show();

            // Replace middle Text
            currentText.hide();
            warumTransparenzText.show();
            currentText = warumTransparenzText;

            warumTransparentHover.css('cursor', 'pointer');
            warumTransparentHover.click(function() {
                location.href = warumTransparenzURL;
            });
            warumTransparentHover.mouseout(function() {
                warumTransparentHover.hide();
                warumTransparentHover.unbind('click');
            });
        });

        warumTransparenzColor.mouseover(function() {
            // Replace middle Text
            currentText.hide();
            warumTransparenzText.show();
            currentText = warumTransparenzText;
        });

        warumSicherSW.mouseover(function() {
            warumSicherHover.show();

            // Replace middle Text
            currentText.hide();
            warumSicherText.show();
            currentText = warumSicherText;

            warumSicherHover.css('cursor', 'pointer');
            warumSicherHover.click(function() {
                location.href = warumSicherURL;
            });
            warumSicherHover.mouseout(function() {
                warumSicherHover.hide();
                warumSicherHover.unbind('click');
            });
        });

        warumSicherColor.mouseover(function() {
            // Replace middle Text
            currentText.hide();
            warumSicherText.show();
            currentText = warumSicherText;
        });

        warumFlexibelSW.mouseover(function() {
            warumFlexibelHover.show();

            // Replace middle Text
            currentText.hide();
            warumFlexibelText.show();
            currentText = warumFlexibelText;

            warumFlexibelHover.css('cursor', 'pointer');
            warumFlexibelHover.click(function() {
                location.href = warumFlexibelURL;
            });
            warumFlexibelHover.mouseout(function() {
                warumFlexibelHover.hide();
                warumFlexibelHover.unbind('click');
            });
        });

        warumFlexibelColor.mouseover(function() {
            // Replace middle Text
            currentText.hide();
            warumFlexibelText.show();
            currentText = warumFlexibelText;
        });

    });

}

function initBuyEasyfolioLinks() {
    var $unsereKooperationspartner = $('#unsere-kooperationspartner');
    if($unsereKooperationspartner.length) {
        $unsereKooperationspartner.find('input[type=radio]').iCheck();
        $unsereKooperationspartner.find('.buy').click(function() {
            var $button = $(this);
            window.open($button.parent().find('input:checked').attr('data-link'), '_blank');
        });
    }
    var $weitereBroker = $('#weitere-broker');
    if($weitereBroker.length) {
        $weitereBroker.find('input[type=radio]').iCheck();
        $weitereBroker.find('.buy').click(function() {
            var $button = $(this);
            window.open($button.parent().find('input:checked').attr('data-link'), '_blank');
        });
    }
}

function initEasy30() {
    if($('body').hasClass("easy30")) {
        initEasy30DynamicData();
        initEasy30Chart();
        initEasy30PortfolioAllocation();
        initEasy30StaticData();
    }
}

function initEasy50() {
    if($('body').hasClass("easy50")) {
        initEasy50DynamicData();
        initEasy50Chart();
        initEasy50PortfolioAllocation();
        initEasy50StaticData();
    }
}

function initEasy70() {
    if($('body').hasClass("easy70")) {
        initEasy70DynamicData();
        initEasy70Chart();
        initEasy70PortfolioAllocation();
        initEasy70StaticData();
    }
}

function initEasy30DynamicData() {
    var getEasy30DynamicDataUrl = "http://dev.api.easyfolio.de/easy30/dynamicData/?format=json";
    $.getJSON(getEasy30DynamicDataUrl, function(data) {
        console.log(data);
        showDynamicData(data);
    });
}

function initEasy50DynamicData() {
    var getEasy50DynamicDataUrl = "http://dev.api.easyfolio.de/easy50/dynamicData/?format=json";
    $.getJSON(getEasy50DynamicDataUrl, function(data) {
        console.log(data);
        showDynamicData(data);
    });
}

function initEasy70DynamicData() {
    var getEasy70DynamicDataUrl = "http://dev.api.easyfolio.de/easy70/dynamicData/?format=json";
    $.getJSON(getEasy70DynamicDataUrl, function(data) {
        console.log(data);
        showDynamicData(data);
    });
}

function showDynamicData(data) {
    var $circle = $('.circle');
    $circle.find('.date').empty().append(moment(data.date).format('DD.MM.YYYY'));
    $circle.find('.currentRate').empty().append((data.value).formatMoney(2, ",", ".") + " EUR");
    $circle.find('.changeInEur').empty().append((data.changeInEur).formatMoney(2, ",", ".") + " â‚¬");
    $circle.find('.changeInPercent').empty().append((data.changeInPercent).formatMoney(2, ",", ".") + "%");

    var $legend = $('.legend');
    $legend.find('.portfolioCostsValue').empty().append((data.cost).formatMoney(2, ",", ".") + "%");
}

function initEasy30Chart() {
    var getEasy30PerformanceDataUrl = "http://dev.api.easyfolio.de/easy30/performanceData/?format=json";
    $.getJSON(getEasy30PerformanceDataUrl, function(data) {
        console.log(data);
        showChart(data, "easyfolio 30 Index");
    }).error(function(error) {
        console.log(error);
    });
}

function initEasy50Chart() {
    var getEasy50PerformanceDataUrl = "http://dev.api.easyfolio.de/easy50/performanceData/?format=json";
    $.getJSON(getEasy50PerformanceDataUrl, function(data) {
        console.log(data);
        showChart(data, "easyfolio 50 Index");
    }).error(function(error) {
        console.log(error);
    });
}

function initEasy70Chart() {
    var getEasy70PerformanceDataUrl = "http://dev.api.easyfolio.de/easy70/performanceData/?format=json";
    $.getJSON(getEasy70PerformanceDataUrl, function(data) {
        console.log(data);
        showChart(data, "easyfolio 70 Index");
    }).error(function(error) {
        console.log(error);
    });
}

function showChart(data, indexName) {

    var $timeFrame = $('.timeFrame');
    $timeFrame.customSelect();
    var chart;

    $timeFrame.change(function() {
        var timeFrame = $(this).val();
        var today = new Date();
        if(timeFrame == 'total') {
            chart.xAxis[0].setExtremes(null, null);
        } else if(timeFrame == '1Month') {
            chart.xAxis[0].setExtremes(Date.UTC(today.getFullYear(), today.getMonth() -1, today.getDate()), Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()));
        } else if(timeFrame == '3Months') {
            chart.xAxis[0].setExtremes(Date.UTC(today.getFullYear(), today.getMonth() -3, today.getDate()), Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()));
        } else if(timeFrame == '6Months') {
            chart.xAxis[0].setExtremes(Date.UTC(today.getFullYear(), today.getMonth() -6, today.getDate()), Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()));
        } else if(timeFrame == '1Year') {
            chart.xAxis[0].setExtremes(Date.UTC(today.getFullYear() -1, today.getMonth(), today.getDate()), Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()));
        } else if(timeFrame == '3Years') {
            chart.xAxis[0].setExtremes(Date.UTC(today.getFullYear() - 3, today.getMonth() -6, today.getDate()), Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()));
        } else if(timeFrame == '5Years') {
            chart.xAxis[0].setExtremes(Date.UTC(today.getFullYear() - 5, today.getMonth() -6, today.getDate()), Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()));
        } else if(timeFrame == 'currentYear') {
            chart.xAxis[0].setExtremes(Date.UTC(today.getFullYear(), 0, 1), Date.UTC(today.getFullYear(), today.getMonth(), today.getDate()));
        }
    });

    var series = [];
    $.each(data, function(index, entry) {
        series.push([Date.UTC(entry[0], entry[1], entry[2]), parseFloat(entry[3])]);
    });
    var $chartContainer = $('#chart');
    if($chartContainer.length > 0) {
        chart = new Highcharts.StockChart({
            chart: {
                zoomType: 'x',
                spacingRight: 2,
                renderTo: 'chart',
                backgroundColor: '#efefef'
            },
            xAxis: {
                type: 'datetime'
            },
            colors: [
                '#91b419'
            ],
            navigator: {
                enabled: false
            },
            rangeSelector: {
                enabled: false
            },
            scrollbar: {
                enabled: false
            },
            plotOptions: {
                line: {
                    lineWidth: 2,
                    marker: {
                        enabled: false
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                formatter: function() {
                    return weekdayNames[moment(this.x).format("d")] + " " + moment(this.x).format("DD.MM.YYYY") + "<br />" + indexName + ": " + this.y.formatMoney(2, ",", ".");
                }
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                type: 'line',
                data: series
            }]
        });
    }

}

function initEasy30PortfolioAllocation() {
    var getEasy30PortfolioAllocationUrl = "http://dev.api.easyfolio.de/easy30/portfolioAllocation/?format=json";
    $.getJSON(getEasy30PortfolioAllocationUrl, function(data) {
        console.log(data);
        showPortfolioAllocation(data);
    });
}

function initEasy50PortfolioAllocation() {
    var getEasy50PortfolioAllocationUrl = "http://dev.api.easyfolio.de/easy50/portfolioAllocation/?format=json";
    $.getJSON(getEasy50PortfolioAllocationUrl, function(data) {
        console.log(data);
        showPortfolioAllocation(data);
    });
}

function initEasy70PortfolioAllocation() {
    var getEasy70PortfolioAllocationUrl = "http://dev.api.easyfolio.de/easy70/portfolioAllocation/?format=json";
    $.getJSON(getEasy70PortfolioAllocationUrl, function(data) {
        console.log(data);
        showPortfolioAllocation(data);
    });
}

function showPortfolioAllocation(data) {

    var series = [];

    var bondColors = [
        '#606f2e',
        '#7e9434',
        '#a1bf3b',
        '#b2cb5e',
        '#c3d681',
        '#d0e38e',
        '#ddeea1',
        '#e6f1c1',
        '#ecf3d5',
        '#f3f7e7'
    ];
    var stockColorCounter = 0;
    var stockColors = [
        '#465b77',
        '#516a8c',
        '#587aa8',
        '#95abc9',
        '#acbdd5',
        '#c4d1e4',
        '#d1ddee',
        '#e1e8f3',
        '#ebf0f7',
        '#f5f8fc'
    ];
    var bondColorCounter = 0;

    var totalWeight = 0;
    var stocksWeight = 0;
    var bondsWeight = 0;
    var otherWeight = 0;
    $.each(data, function(index, part) {
        if(part == null) {
            return;
        }
        totalWeight += part.weight;
        if(part.assetType == "stocks") {
            stocksWeight += part.weight;
        } else if(part.assetType == "bonds") {
            bondsWeight += part.weight;
        } else {
            otherWeight += part.weight;
        }
    });

    var $legend = $('.legend');
    $legend.find('.stocks .value').empty().append((stocksWeight / totalWeight * 100).formatMoney(2, ",", ".") + "%");
    $legend.find('.bonds .value').empty().append((bondsWeight / totalWeight * 100).formatMoney(2, ",", ".") + "%");
    $legend.find('.other .value').empty().append((otherWeight / totalWeight * 100).formatMoney(2, ",", ".") + "%");

    $.each(data, function(index, part) {
        if(part == null) {
            return;
        }
        var newElement = {};
        newElement.y = part.weight;
        newElement.name = part.name;
        if(part.assetType == "stocks") {
            newElement.color = stockColors[stockColorCounter];
            stockColorCounter++;
        } else if(part.assetType == "bonds") {
            newElement.color = bondColors[bondColorCounter];
            bondColorCounter++;
        } else {
            newElement.color = '#e0e0e0';
        }

        series.push(newElement);
    });

    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'portfolioAllocationGraph',
            type: 'pie'
        },
        xAxis: {
            enabled: false
        },
        title: {
            text: null
        },
        plotOptions: {
            pie: {
                shadow: false,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    distance: -38,
                    formatter: function() {
                        if(this.point.percentage > 1) {
                            return (this.point.percentage).formatMoney(2, ",", ".") + "%";
                        } else {
                            return "";
                        }
                    },
                    style: {
                        color: 'white',
                        fontSize: '13px',
                        fontFamily: "'ITCAvantGardeW01-CnMd', Arial, sans-serif"
                    }
                }
            }
        },
        tooltip: {
            pointFormat: ''
        },
        series: [{
            data: series,
            innerSize: '60%'
        }],
        credits: {
            enabled: false
        }
    });
}

function initEasy30StaticData() {
    var getEasy30StaticDataUrl = "http://dev.api.easyfolio.de/easy30/staticData/?format=json";
    $.getJSON(getEasy30StaticDataUrl, function(staticData) {
        console.log(staticData);
        replaceStaticDataPlaceholder(staticData);
    });
}

function initEasy50StaticData() {
    var getEasy50StaticDataUrl = "http://dev.api.easyfolio.de/easy50/staticData/?format=json";
    $.getJSON(getEasy50StaticDataUrl, function(staticData) {
        console.log(staticData);
        replaceStaticDataPlaceholder(staticData);
    });
}

function initEasy70StaticData() {
    var getEasy70StaticDataUrl = "http://dev.api.easyfolio.de/easy70/staticData/?format=json";
    $.getJSON(getEasy70StaticDataUrl, function(staticData) {
        console.log(staticData);
        replaceStaticDataPlaceholder(staticData);
    });
}

function replaceStaticDataPlaceholder(staticData) {
    for(var i = 0; i < staticData.length; i++) {
        var attribute = staticData[i].attribute;
        var value = staticData[i].value;
        var search = "{-" + attribute + "-}";
        console.log($('h1:contains(' + search + '),h2:contains(' + search + '),h3:contains(' + search + '),p:contains(' + search + '),dt:contains(' + search + '),dd:contains(' + search + ')'));
        $('h1:contains(' + search + '),h2:contains(' + search + '),h3:contains(' + search + '),p:contains(' + search + '),dt:contains(' + search + '),dd:contains(' + search + ')').each(function() {
            var text = $(this).text();
            $(this).text(text.replace(search, value));
        });
    }
    initFondsSlider();
}

function initNewsletterPopup() {
    setTimeout(function() {
        if($.cookie('newsletterPopupShown') === undefined) {
            $('#newsletterPopup').modal();
            $.cookie('newsletterPopupShown', true, { expires: 30, path: '/', domain: '.easyfolio.drve.com' });
        }
    }, 40000);
}

function initFormCheckboxes() {
    $('form input[type=checkbox]').iCheck();
}

function initNewsletterSlide() {
    var $financialAdvisorButton = $('.button.financialAdvisor');
    if($financialAdvisorButton.length > 0) {
        $financialAdvisorButton.click(function() {
            var $finanzberater = $('.mod_article#newsletter-finanzberater');
            $finanzberater.slideToggle(500);
            scrollToElement($finanzberater, 100);
        });
    }
}

function scrollToElement($element, offset) {
    if(offset == undefined) {
        offset = 0;
    }
    $('html, body').animate({
        scrollTop: $element.offset().top - offset
    }, 1000);
}

function initMobileNavigation() {
    var $navigationItems = $('header .navbar li.dropdown a[data-toggle]');
    $navigationItems.click(function() {
        var $item = $(this);
        if($item.parent().hasClass("open")) {
            location.href = $item.attr('href');
        }
    });
}

function initQuestionnaire() {
    var $questionnaire = $('#questionnaire');
    if($questionnaire.length == 0) {
        return;
    }
    var questionnaire = $questionnaire.questionnaire({
        version: 0.5,
        showQuestionNo: 1, // 1: Showing from question 1, not mandatory.
        answers: [
          {'questionNo': 2, 'answerNo': 1},
          {'questionNo': 1, 'answerNo': 2},
        ],
        onInitialized: function(version, latestVersion){
          console.log('Questionnaire initialized with version {0} and the latestVersion is {1}'.format(
            version, latestVersion)
          );
        },
        onQuestionAnswered: function(riskLevel, riskClass, questionNo){
          console.log('Question {0} answered. riskLevel: {1}, riskClass: {2}'.format(
            questionNo, riskLevel, riskClass
          ));
        },
        onAllQuestionsAnswered: function(riskLevel, riskClass, answers){
          console.log('All Question Answered:', riskLevel, riskClass, answers);
        }
    });
}

Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};