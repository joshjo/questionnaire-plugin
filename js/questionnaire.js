String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+i+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};
(function($){
    $.questionnaire = function(el, options){
        this.options = options;
        this.ajax_url = 'http://localhost:8000/';
        this.$el = $(el);
        this.$questionnaire_el = $('<form>', {'class':'questionnaire_form'});
        this.version = this.$el.data('version');
        this.risk_level = this.$el.data('risk_level');
        this.risk_class = this.$el.data('risk_class');
        this.is_initialezed = this.$el.data('is_initialized');
        this.init();
    };
    $.questionnaire.prototype = {
        init: function(){
            var base = this,
                settings,
                $el = base.$el,
                el = $el[0];
            if (base.is_initialezed === true)
                return this;
            base.settings = settings = $.extend({}, base.defaults, base.options);
            function ajax_widget(version, showQuestionNo, answers){
                return $.ajax({
                    url: base.ajax_url.concat('questionnaire/ajax_widget/'),
                    data:{
                        'version': settings.version,
                        'showQuestionNo': settings.showQuestionNo,
                        'answers': JSON.stringify(settings.answers),
                    },
                    async: false,
                });
            }
            ajax_widget().success(function(data){
                base.$el.data("latest_version", data["latest_version"]);
                data = data['questionnaire'];
                var question_number = 0;
                for (var i=0; i<data.length; ++i){
                    base.$el.append($('<span>', {
                        'color': '#dddddd',
                        'class': 'category'
                    }).append($('<span>', {
                        text: data[i].category,
                    })));
                    questions = data[i].questions;
                    for (var question in questions){
                        var dom_category = $('<div>', {
                            'class': 'question'
                        });
                        dom_category.append($('<div>',{
                            'class': 'question-bg',
                            'text': 'Your browser does not support the HTML5 canvas tag.'
                        }));
                        var q_answers = questions[question];
                        var $dom_anwers = $('<div>', {
                            'class': 'question-content'
                        }).append($('<div>',{
                            text: data[i].name_questions[question],
                            'class': 'question-question'
                        }));
                        var $form_answers = $('<form>', {
                            'action': '',
                            'class': 'options'
                        });
                        var $question_anwers = $('<div>',
                            { 'class': 'question-answers' }
                        );
                        for (var j=0; j<q_answers.length; ++j){
                            var _class = '';
                            var _id = '';
                            if (question == '10')
                                _class = 'non-exception-option';
                            if (j === 0 && question == '10'){
                                _id = 'exception-option';
                                _class = '';
                            }
                            var checked = false;
                            if (q_answers[j][2] == 'checked') checked = true;
                            var $input_radio = $('<input>', {
                                'type': 'radio',
                                'name': 'question_' + (question_number+1),
                                'value': q_answers[j][0],
                                'risk': q_answers[j][3],
                                'alternative': j+1,
                                'checked': checked,
                                'class': _class,
                                'id': _id
                            });
                            $form_answers.append($('<label>'
                                ).append($input_radio
                                ).append(q_answers[j][1])
                            );
                        }
                        $question_anwers.append($form_answers);
                        question_number++;
                        dom_category.append($dom_anwers).trigger('create');
                        dom_category.append($question_anwers);
                        base.$el.append(dom_category);
                    }
                }
            });
            $("input[type='radio']").checkboxradio();
            $("input[type='radio']").checkboxradio("refresh");
            base.$el.data("version", settings.version);
            base.$el.data("is_initialized", true);
            init_demonstration();
            if ( $.isFunction( settings.onInitialized ) ) {
                settings.onInitialized.call(this,
                    settings.version,
                    base.$el.data("latest_version")
                );
            }
            var $options_form = base.$el.find($('.options'));
            $('input[type=radio]', $options_form).click(function(){
                base.risk();
                var $question_answered = $(this).attr('name').substring(9);
                var $alternative_answered = $(this).attr('alternative');
                var $questions_anwwered = $('input[type=radio]:checked',
                    $options_form
                );
                var $len_checked_questions = $questions_anwwered.length;
                var $len_questions = $options_form.length;
                if ($len_questions == $len_checked_questions && $.isFunction(
                        settings.onAllQuestionsAnswered )){
                    var answers = [];
                    $questions_anwwered.each(function(i, elem){
                        var alternative = parseInt($(elem).attr('alternative'));
                        var question_no = parseInt(
                            $(elem).attr('name').substring(9)
                        );
                        answers.push({
                            'questionNo': question_no,
                            'answerNo': alternative
                        });
                    });
                    settings.onAllQuestionsAnswered.call(this,
                        base.$el.data('risk_level'),
                        base.$el.data('risk_class'),
                        answers
                    );
                }
                if ( $.isFunction( settings.onQuestionAnswered ) ) {
                    settings.onQuestionAnswered.call(this,
                        base.$el.data('risk_level'),
                        base.$el.data('risk_class'),
                        parseInt($question_answered),
                        parseInt($alternative_answered)
                    );
                }
            });
        },
        defaults: {
            categoryClass: 'category',
            categoryTitleClass: 'category_title',
            version: 2,
            answers: [
                {'questionNo': 12, 'answerNo': 1},
            ],
            showQuestionNo: 1,
            onInitialized: null,
            onQuestionAnswered: null,
            onAllQuestionsAnswered: null,
        },
        risk: function(){
            var base = this;
            var $options_form = base.$el.find($('.options'));
            var answered_pk_array = [];
            $('input[type=radio]:checked', $options_form).each(function(i, elem){
                answered_pk_array[i] = $(elem).attr('risk');
            });
            $.ajax({
                url: base.ajax_url.concat('questionnaire/ajax_get_risk/'),
                data:{
                    'answered_pk_array': JSON.stringify(answered_pk_array),
                    'version': base.$el.data('version')
                },
                async: false,
            }).success(function(data){
                base.$el.data('risk_level', data.risk_level);
                base.$el.data('risk_class', data.risk_class);
            });
        },
    };
    $.fn.questionnaire = function(options){
        var $instance = new $.questionnaire(this, options);
        if ( options === 'object' || ! options ) {
            return this.each(function(){
                $instance;
            });
        }
        else if ($instance[options]){
            if ($.isFunction($instance[options])){
                return $instance[options].apply($instance, Array.prototype.slice.call(arguments, 1));
            }
            return $instance[options];
        }
        else{
            // $.error('Method ' + options + " doesn't exist on jQuery.questionnaire");
        }
    };
}(jQuery));